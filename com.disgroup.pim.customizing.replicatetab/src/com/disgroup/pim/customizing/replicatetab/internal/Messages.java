package com.disgroup.pim.customizing.replicatetab.internal;

import java.text.MessageFormat;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.heiler.ppm.commons.util.MessagesHelper;

public class Messages {
	private static final String BUNDLE_NAME = "com.disgroup.pim.customizing.replicatetab.internal.messages"; //$NON-NLS-1$

	private static final Log log = LogFactory.getLog(Messages.class);

	private static final MessagesHelper messagesHelper = new MessagesHelper(BUNDLE_NAME, // $NON-NLS-1$
			Messages.class);

	public static String getString(String key, String... arg) {
		String res = messagesHelper.getString(key);
		if (arg.length > 0)
			try {
				return MessageFormat.format(res, (Object[]) arg);
			} catch (Exception e) {
				log.error("Error getting message", e);
				return res;
			}
		return res;

	}
}
