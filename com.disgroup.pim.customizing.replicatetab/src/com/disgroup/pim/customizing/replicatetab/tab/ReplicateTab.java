package com.disgroup.pim.customizing.replicatetab.tab;

import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.ecore.sdo.EDataObject;

import com.disgroup.pim.customizing.replicatetab.internal.Messages;
import com.heiler.ppm.repository.Entity;
import com.heiler.ppm.repository.path.EntityPath;
import com.heiler.ppm.repository.path.FieldPath;
import com.heiler.ppm.std.core.command.CommandContext;
import com.heiler.ppm.std.core.command.CommandContextFactory;
import com.heiler.ppm.std.core.command.PutCommand;
import com.heiler.ppm.std.core.entity.CommandContextParam;
import com.heiler.ppm.std.core.entity.EntityDetailModel;
import com.heiler.ppm.std.core.entity.EntityProxy;
import com.heiler.ppm.std.core.entity.filter.LoadHint;
import com.heiler.ppm.std.core.entity.filter.LoadHintBuilder;
import com.heiler.ppm.web.common.view.detail.AbstractEntityDetailTab;
import com.heiler.ppm.web.common.view.detail.DetailModelUpdateListener;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

public class ReplicateTab extends AbstractEntityDetailTab {
	private static final Log LOG = LogFactory.getLog(ReplicateTab.class);

	private static final String PLANT_LK = "ArticleDomainType.LK.Std_LK_Text100_01";

	private static final String PLANT_IDENTIFIER = "std_LK_Text100_01";

	private static final String DISPATCH_MARK_IDENTIFIER = "std_Bit_01";

	private static final String DISPATCH_MARK_FIELD_NAME = "ArticlePurchasePlant.DispatchMark";

	private static final short ARTICLE_PURCHASE_PLANT_ENTITY_ID = 21800;

	private Entity articleEntity = null;

	private Entity articlePurchasePlantEntity = null;

	private EntityPath articlePurchasePlantEntityPath = null;

	private LoadHint loadHint = null;

	VerticalLayout verticalLayout = null;

	@Override
	protected void onPostConstruct() {
		super.onPostConstruct();
		articleEntity = repositoryService.getEntityByIdentifier("Article");
		articlePurchasePlantEntity = articleEntity.getEntity("ArticlePurchasePlant", true);
		articlePurchasePlantEntityPath = new EntityPath(articlePurchasePlantEntity.getEntityType());
		loadHint = new LoadHintBuilder(articleEntity.getEntityType()).add(articlePurchasePlantEntityPath).build();
		verticalLayout = new VerticalLayout();
		this.addListener((DetailModelUpdateListener<EntityProxy>) listener -> {
			LOG.debug("Detail model update listener fired");
			buildMainLayout();
		});
	}

	@Override
	protected void buildMainLayout() {
		LOG.debug("Building replicate layout");
		this.verticalLayout.removeAllComponents();
		Label label = new Label(Messages.getString("web.article.detail.tab.replicate.caption", ""));
		List<EDataObject> eDataObjects = null;
		EntityDetailModel entityDetailModel = null;
		try {
			entityDetailModel = this.model.getDetailModel(loadHint);
			eDataObjects = entityDetailModel.getDataObject().getList("articleDomain");
		} catch (CoreException e) {
			LOG.error("Error in building replicate tab");
			e.printStackTrace();
			return;
		}
		if (eDataObjects == null || entityDetailModel == null) {
			LOG.error("entityDetailModel is null or eDataObjects is null!");
			return;
		}
		verticalLayout.setMargin(true);
		verticalLayout.addComponent(label);
		verticalLayout.addComponent(new Label("<hr/>", ContentMode.HTML));
		final EntityDetailModel finalEntityDetailModel = entityDetailModel;

//		OptionGroup multiChoice = new OptionGroup(Messages.getString("web.article.detail.tab.replicate.optionGroup", ""));
//		multiChoice.setMultiSelect(true);

		CheckBox button = new CheckBox(Messages.getString("web.article.detail.tab.replicate.button.selectAll", ""));
		button.setId("selectAll");
		button.addValueChangeListener(listener -> {
			Iterator<Component> iterator = verticalLayout.iterator();
			while (iterator.hasNext()) {
				Component component = iterator.next();
				if (!(component instanceof CheckBox) || "selectAll".equals(component.getId())) {
					continue;
				}
				CheckBox checkBox = (CheckBox) component;
				checkBox.setValue(button.getValue());
			}
		});
		verticalLayout.addComponent(button);
		verticalLayout.addComponent(new Label("<hr/>", ContentMode.HTML));
		eDataObjects.stream().filter(domain -> domain.getShort("entityId") == ARTICLE_PURCHASE_PLANT_ENTITY_ID)
				.forEach(articlePurchasePlant -> {
					String plant = articlePurchasePlant.getString(PLANT_IDENTIFIER);
					CheckBox checkBox = new CheckBox(
							Messages.getString("web.article.detail.tab.replicate.plantNo", plant));
					checkBox.setValue(articlePurchasePlant.getBoolean(DISPATCH_MARK_IDENTIFIER));
					checkBox.addValueChangeListener(event -> {
						try {
							setValue(finalEntityDetailModel, plant, checkBox.getValue());
						} catch (CoreException e) {
							LOG.error("Error in assigninig checkbox value!");
							e.printStackTrace();
						}
					});
					verticalLayout.addComponent(checkBox);
				});

		setCompositionRoot(verticalLayout);
		LOG.debug("Finished rendering");
	}

	private void setValue(EntityDetailModel entityDetailModel, String plant, boolean value) throws CoreException {
		LOG.debug("Setting checkbox value");
		CommandContextParam commandContextParam = new CommandContextParam(entityDetailModel);

		CommandContext commandContext = CommandContextFactory.createContext(commandContextParam);

		articlePurchasePlantEntityPath.setLogicalKeyValue(PLANT_LK, plant);

		FieldPath underFieldPath = new FieldPath(articlePurchasePlantEntityPath,
				articlePurchasePlantEntity.getField(DISPATCH_MARK_FIELD_NAME).getFieldType());

		PutCommand putCommand = (PutCommand) commandContext.getCommand(PutCommand.TYPE);

		entityDetailModel.acquireWrite(null);
		putCommand.put(commandContext, entityDetailModel.getDataObject(), entityDetailModel.getDataObject(),
				articlePurchasePlantEntity, underFieldPath, value, true);
		entityDetailModel.save(null);
		entityDetailModel.releaseWrite();
	}

	@Override
	protected void afterModelUpdate(boolean isModelUpdate) throws CoreException {
		LOG.debug("Model update triggered.");
//		if (isModelUpdate) {
//			LOG.debug("Model has been updated.");
//			this.buildMainLayout();
//		}
//		this.buildMainLayout();
		if (this.model != null) {
			buildMainLayout();
		}

	}

	@Override
	public String getDisplayPermissionId() {
		// TODO Auto-generated method stub
		return "web.article.detail.tab.replicate";
	}

	@Override
	public boolean hasReadPermission() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public int getPosition() {
		// TODO Auto-generated method stub
		return 11;
	}

	@Override
	public String getI18nKey() {
		// TODO Auto-generated method stub
		return Messages.getString("web.article.detail.tab.replicate", "");
	}
}
