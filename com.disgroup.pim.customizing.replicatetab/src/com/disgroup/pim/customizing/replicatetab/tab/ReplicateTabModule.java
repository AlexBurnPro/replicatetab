package com.disgroup.pim.customizing.replicatetab.tab;

import com.google.inject.multibindings.Multibinder;
import com.heiler.ppm.std.core.entity.EntityProxy;
import com.heiler.ppm.web.common.integration.AbstractWebModule;
import com.heiler.ppm.web.common.view.detail.DetailTab;

public class ReplicateTabModule extends AbstractWebModule {
	@Override
	protected void configure() {
		Multibinder<DetailTab<EntityProxy>> detailBinder = getDetailTabBinder("article.detailtab");
		
		detailBinder.addBinding().to(ReplicateTab.class);
		
	}


}
